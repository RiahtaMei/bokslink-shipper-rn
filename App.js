//App.js
import React from 'react'
import AppContainer from './android/navigation'

export default function App() {
  return <AppContainer />
}