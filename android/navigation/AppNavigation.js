//AppNavigation.js
import React, { Component } from 'react';
import { View, Text, StyleSheet, Button } from 'react-native';
// import Icon from '@expo/vector-icons/Ionicons';
import Icon from 'react-native-vector-icons/Ionicons';
// import Icon from 'react-native-ionicons';



import { createStackNavigator } from 'react-navigation-stack'
import Home from '../screens/Home'
import Home2 from '../screens/Home2'
import Home3 from '../screens/Home3'
import Home4 from '../screens/Home4'
import Form from '../screens/Form'
import DetailBarang from '../screens/DetailBarang'
import OrderDetail from '../screens/OrderDetail'
import Konfirmasi from '../screens/Konfirmasi'
import DetailPengiriman from '../screens/DetailPengiriman'



import { createTabNavigator, createBottomTabNavigator } from 'react-navigation-tabs'
import { createDrawerNavigator } from 'react-navigation-drawer'


// const AppNavigation = createStackNavigator(
//   {
//     Home: { screen: Home }
//   },
//   {
//     initialRouteName: 'Home',
//     headerMode:'none'
//   }
// )

const HomeStack = createStackNavigator(
    {
      Home: { screen: Home },
      Form: { screen: Form  },
      DetailBarang: { screen: DetailBarang },
      OrderDetail: { screen: OrderDetail },
      Konfirmasi: { screen: Konfirmasi },
      DetailPengiriman: { screen: DetailPengiriman }
    },
    {
      initialRouteName: 'Home',
      headerMode:'none'
    }
  )

const TabHomeNavigator = createBottomTabNavigator(
    {
        // Home: HomeStack,
        Home: {
            screen: HomeStack,
            navigationOptions: () => ({
                tabBarIcon: ({tintColor}) => (
                    <Icon
                        name="md-home"
                        color={ tintColor }
                        size={24}
                    />
                )
            })
        },
        Home2: {
            screen: Home2,
            navigationOptions: () => ({
                tabBarIcon: ({tintColor}) => (
                    <Icon
                        name="md-notifications"
                        color={ tintColor }
                        size={24}
                    />
                )
            })
        },
        // Home3: Home3
        Home3: {
            screen: Home3,
            navigationOptions: () => ({
                tabBarIcon: ({tintColor}) => (
                    <Icon
                        name="md-paper-plane"
                        color={ tintColor }
                        size={24}
                    />
                )
            })
        },
    },
    {
        headerMode:'none',
        backgroundColor: '#171F33' // TabBar background
    }    
)


const DashboardStackNavigator = createStackNavigator(
    {
      DashboardTabNavigator: TabHomeNavigator
    },
    {
      defaultNavigationOptions: ({ navigation }) => {
        return {
          headerLeft: (
            <Icon style={{ paddingLeft: 10 }} onPress={() => navigation.openDrawer()} name="md-menu" size={30} />
          )
        };
      }
    },
    {
        headerMode:'none'
    }
  );

const AppDrawerNavigator = createDrawerNavigator(
    {
    Dashboard: {
      screen: DashboardStackNavigator,
    },
    Home4: {
        screen: Home4
      }
  },
  {
    headerMode:'none'
  });

// export default AppNavigation

// export default TabHomeNavigator

export default AppDrawerNavigator