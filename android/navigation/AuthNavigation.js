//AuthNavigation.js
import { createStackNavigator } from 'react-navigation-stack'
import Login from '../screens/Login'

import Signup from '../screens/Signup'
import Verifikasi from '../screens/Verifikasi'

import LupaPassword from '../screens/LupaPassword'
import OTPpemulihan from '../screens/OTPpemulihan'
import GantiPassword from '../screens/GantiPassword'

const AuthNavigation = createStackNavigator(
  {
    Login: { screen: Login },

    Signup: { screen: Signup },
    Verifikasi: { screen: Verifikasi },

    LupaPassword: { screen: LupaPassword },
    OTPpemulihan: { screen: OTPpemulihan },
    GantiPassword: { screen: GantiPassword }
  },
  {
    initialRouteName: 'Login',
    headerMode: 'none'
  }
)

export default AuthNavigation