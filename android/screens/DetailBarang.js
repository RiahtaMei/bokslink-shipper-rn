import React, { Fragment, Component } from 'react';
import ImagePicker from 'react-native-image-picker';
import {
  SafeAreaView,
  StyleSheet,
  ScrollView,
  View,
  StatusBar,
  Image,
  Button,
  Dimensions,
  TouchableOpacity, 
  KeyboardAvoidingView,
  TextInput
} from 'react-native';

import {
  Header,
  LearnMoreLinks,
  Colors,
  DebugInstructions,
  ReloadInstructions,
} from 'react-native/Libraries/NewAppScreen';


import { Dropdown } from 'react-native-material-dropdown';
import { Block, Text } from 'galio-framework';


const options = {
  title: 'Select Avatar',
  customButtons: [{ name: 'fb', title: 'Choose Photo from Facebook' }],
  storageOptions: {
    skipBackup: true,
    path: 'images',
  },
};
export default class DetailBarang extends Component {
  constructor(props) {
    super(props)
    this.state = {
      filepath: {
        data: '',
        uri: ''
      },
      fileData: '',
      fileUri: '',
      kategori_barang:'',
      sub_kategori: '',
      deskripsi: '',
      jumlah_barang: '',
      total_harga: '',
      panjang: '',
      lebar: '',
      tinggi: '',
      berat: '',
      berat_setara: ''
    }
  }

  chooseImage = () => {
    let options = {
      title: 'Select Image',
      customButtons: [
        { name: 'customOptionKey', title: 'Choose Photo from Custom Option' },
      ],
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.showImagePicker(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };

        // You can also display the image using data:
        // const source = { uri: 'data:image/jpeg;base64,' + response.data };
        // alert(JSON.stringify(response));s
        console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri
        });
      }
    });
  }

  launchCamera = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchCamera(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri
        });
      }
    });

  }

  launchImageLibrary = () => {
    let options = {
      storageOptions: {
        skipBackup: true,
        path: 'images',
      },
    };
    ImagePicker.launchImageLibrary(options, (response) => {
      console.log('Response = ', response);

      if (response.didCancel) {
        console.log('User cancelled image picker');
      } else if (response.error) {
        console.log('ImagePicker Error: ', response.error);
      } else if (response.customButton) {
        console.log('User tapped custom button: ', response.customButton);
        alert(response.customButton);
      } else {
        const source = { uri: response.uri };
        console.log('response', JSON.stringify(response));
        this.setState({
          filePath: response,
          fileData: response.data,
          fileUri: response.uri
        });
      }
    });

  }

  renderFileData() {
    if (this.state.fileData) {
      return <Image source={{ uri: 'data:image/jpeg;base64,' + this.state.fileData }}
        style={styles.images}
      />
    } else {
      return <Image source={require('../Image/dummy.png')}
        style={styles.images}
      />
    }
  }

  renderFileUri() {
    if (this.state.fileUri) {
      return <Image
        source={{ uri: this.state.fileUri }}
        style={styles.images}
      />
    } else {
      return <Image
        source={require('../Image/dummy.png')}
        style={styles.images}
      />
    }
  }


  goOrderDetail = () => this.props.navigation.navigate('OrderDetail')


  render() {


    let data = [{
      value: 'Dokumen Berharga',
    }, {
      value: 'Dokumen Biasa',
    }, {
      value: 'Paket',
    }, {
      value: 'Lainnya'
    }];
  
    let data2 = [{
      value: 'BPKB Mobil',
    }, {
      value: 'BPKB Motor',
    }, {
      value: 'STNK Mobil',
    }, {
      value: 'STNK Motor',
    }, {
      value: 'Ijazah',
    }, {
      value: 'Sertifikat Tanah Asli',
    }, {
      value: 'Pasport'
    }];


    return (
      <ScrollView style={{backgroundColor:'white'}}>
      <KeyboardAvoidingView enabled behavior={'padding'}>
        <View>
          <Block flex style={styles.container}>
            <StatusBar barStyle="light-content" />
              <Block center>                    
                    <View style={{paddingTop: 10}}>
                    <Dropdown
                      style={{width: 350}}
                      label='Kategori Barang'
                      data={data}
                      value={this.state.kategori_barang}
                      onChangeText={kategori_barang => this.setState({kategori_barang})}   
                    />

                    <Dropdown
                      style={{width: 350}}
                      label='Sub-Kategori Barang'
                      data={data2}
                      disabled = {this.state.kategori_barang != 'Dokumen Berharga'}
                      value={this.state.sub_kategori}
                      onChangeText={sub_kategori => this.setState({sub_kategori})}   
                    />

                      
                        <TextInput style={styles.inputBox4}
                          underlineColorAndroid='rgba(0,0,0,0)'
                          placeholder="Deskripsi Barang"
                          placeholderTextColor = 'black'
                          selectionColor="#fff"
                          numeric value
                          keyboardType={'default'}
                          onChangeText={deskripsi => this.setState({ deskripsi })}
                          value={this.state.deskripsi}
                          multiline={true}
                        />
                        <View style={{flexDirection:'row'}}>
                          <TextInput style={styles.inputBox_3}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Jumlah Barang"
                            placeholderTextColor = 'black'
                            selectionColor="#fff"
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={jumlah_barang => this.setState({ jumlah_barang })}
                            value={this.state.jumlah_barang}
                            maxLength={10}  //setting limit of input
                          />
                          <View style={{paddingLeft: 10}}>
                          <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Total Harga"
                            placeholderTextColor = 'black'
                            selectionColor="#fff"
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={total_harga => this.setState({ total_harga })}
                            value={this.state.total_harga}
                            maxLength={10}  //setting limit of input
                          />
                          </View>
                        </View>

                    </View>

                      <View style={{paddingTop: 10, paddingBottom: 10, alignContent:'center', alignItems:'center', justifyContent:'center'}}>
                          <View style={{borderWidth:1, backgroundColor:'#1faadb', width: 400, height:50,textAlign:'center', justifyContent:'center', alignItems:'center', alignContent:'center', borderColor:'#1faadb'}}>
                              <Text style={{fontWeight:'bold', color:'white', fontSize: 18}}>
                              UKURAN BARANG
                              </Text>
                          </View>
                      </View>

                      <Block flex row>
                        <View style={{paddingRight: 10}}>
                          <TextInput style={styles.inputBox_2}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Panjang (cm)"
                            placeholderTextColor = 'black'
                            selectionColor="#fff"
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={panjang => this.setState({ panjang })}
                            value={this.state.panjang}
                            maxLength={5}  //setting limit of input
                          />  
                        </View>
                        
                        <View style={{paddingRight: 10}}>
                          <TextInput style={styles.inputBox_2}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Lebar (cm)"
                            placeholderTextColor = 'black'
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={lebar => this.setState({ lebar })}
                            value={this.state.lebar}
                            maxLength={5}  //setting limit of input
                          />  
                        </View>

                        <View>
                          <TextInput style={styles.inputBox_2}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Tinggi (cm)"
                            placeholderTextColor = 'black'
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={tinggi => this.setState({ tinggi })}
                            value={this.state.tinggi}
                            maxLength={5}  //setting limit of input
                          />  
                        </View>
                      </Block>
                        
                      <Block flex row>
                        <View style={{paddingRight: 5}}>
                          <TextInput style={styles.inputBox_3}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Berat (kg)"
                            placeholderTextColor = 'black'
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={berat => this.setState({ berat })}
                            value={this.state.berat}
                            maxLength={5}  //setting limit of input
                          />  
                        </View>
                            
                        <View style={{paddingRight: 5}}>
                          <TextInput style={styles.inputBox_3}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            placeholder="Berat Setara (kg)"
                            placeholderTextColor = 'black'
                            numeric value
                            keyboardType={'decimal-pad'}
                            onChangeText={berat_setara => this.setState({ berat_setara })}
                            value={this.state.berat_setara}
                            maxLength={5}  //setting limit of input
                    />  
                  </View>
                </Block>
                
                <View style={styles.ImageSections}>

{/* <View>
  {this.renderFileData()}
  <Text  style={{textAlign:'center'}}>Base 64 String</Text>
</View> */}

<View>
  {this.renderFileUri()}
  {/* <Text style={{textAlign:'center'}}>File Uri</Text> */}
</View>

</View>

<View style={styles.btnParentSection}>
              <TouchableOpacity onPress={this.chooseImage} style={styles.btnSection}  >
                <Text style={styles.btnText}>Choose File</Text>
              </TouchableOpacity>

              {/* <TouchableOpacity onPress={this.launchCamera} style={styles.btnSection}  >
                <Text style={styles.btnText}>Directly Launch Camera</Text>
              </TouchableOpacity>

              <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
                <Text style={styles.btnText}>Directly Launch Image Library</Text>
              </TouchableOpacity> */}
            </View>

                <View style={{paddingTop: 50}}>
                  <TouchableOpacity style={styles.button} onPress={this.goOrderDetail} >
                    <Text style={styles.buttonText}> L A N J U T K A N </Text>
                  </TouchableOpacity>
                </View>
            </Block>
          </Block>  
        </View>
      </KeyboardAvoidingView>     
    </ScrollView>





    );
  }
};

const styles = StyleSheet.create({
  scrollView: { backgroundColor: Colors.lighter, },
  body: { backgroundColor: Colors.white, justifyContent: 'center', borderColor: 'black', borderWidth: 1, height: Dimensions.get('screen').height - 20, width: Dimensions.get('screen').width },
  ImageSections: { display: 'flex', flexDirection: 'row', paddingHorizontal: 8, paddingVertical: 8, justifyContent: 'center' },
  images: { width: 150, height: 150, borderColor: 'black', borderWidth: 1, marginHorizontal: 3 },
  btnParentSection: { alignItems: 'center', marginTop:10 },
  btnSection: { width: 225, height: 50, backgroundColor: '#DCDCDC', alignItems: 'center', justifyContent: 'center', borderRadius: 3, marginBottom:10 },
  btnText: { textAlign: 'center', color: 'gray', fontSize: 14, fontWeight:'bold' },
  container : { flexGrow: 1, justifyContent:'center', alignItems: 'center', backgroundColor: 'white' },
  inputBox: { width:200, height: 40, borderColor:'#1E90FF', borderWidth: 1, borderRadius: 25, paddingHorizontal:10, fontSize:12, color:'black', marginVertical: 5, paddingVertical: 5, textAlign:'center' },
  inputBox_2: { width:110, height: 40, borderColor:'#1E90FF', borderWidth: 1, borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 5, paddingVertical: 5, textAlign: 'center', fontSize: 12 },
  inputBox_3: { width:140, height: 40, borderColor:'#1E90FF', borderWidth: 1, borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 5, paddingVertical: 5, textAlign: 'center', fontSize: 12 },
  inputBox4: { width:350, height: 80, borderColor:'#1E90FF', borderWidth: 1, borderRadius: 25, paddingHorizontal:10, fontSize:14, color:'black', marginVertical: 5, paddingVertical: 5 },
  button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:20, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
});