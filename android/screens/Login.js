//Login.js
import React from 'react'
import { StyleSheet, View, TextInput, Image, TouchableOpacity } from 'react-native'
import { Block, Text } from 'galio-framework';

export default class Login extends React.Component {
  state = {
    email: '',
    password: '',
    userData: {},
    jsonOfItem:'',
    message: 'Email atau Password Kamu salah',
    hidden: true
  }

  onInputLabelPressed = () => {
    this.setState({hidden: !this.state.hidden});
  }


  handleEmailChange = email => {
    this.setState({ email })
  }

  handlePasswordChange = password => {
    this.setState({ password })
  }

  onLogin = async () => {
    const { email, password } = this.state
    try {
      if (email.length > 0 && password.length > 0) {
        this.props.navigation.navigate('App')
      }
    } catch (error) {
      alert(error)
    }
  }

  goToSignup = () => this.props.navigation.navigate('Signup')
  goLupaPassword = () => this.props.navigation.navigate('LupaPassword')

  render() {
    const { email, password } = this.state

    return (
      <View style={styles.container}>

            <Block row center style={{paddingVertical: 40}}> 
                <Image  style={{width: 300, height: 100}} source={require('../Image/logo.png')}/>  
            </Block>

            <Block center>
                  <View style={styles.viewStyle}>
                    <TextInput style={styles.inputBox}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Email"
                      placeholderTextColor = 'grey'
                      keyboardType="email-address"
                      value={this.state.email}
                      onChangeText={email => this.setState({ email })}
                    />
                    <Image source={require('../Image/email.png')} style={styles.ImageStyle} /> 
                  </View>
                  <View style={styles.viewStyle}>
                    <TextInput 
                      style={styles.inputBox}
                      underlineColorAndroid='rgba(0,0,0,0)'
                      placeholder="Kata Sandi"
                      secureTextEntry={this.state.hidden}
                      {...this.props}
                      placeholderTextColor = 'grey'
                      keyboardType='default'
                      value={this.state.password}
                      onChangeText={password => this.setState({ password })}
                    />
                    <TouchableOpacity onPress={this.onInputLabelPressed}>
                      <Image source={require('../Image/pass.png')} style={styles.ImageStyle} />
                    </TouchableOpacity>
                  </View>
            </Block>

            <Block  right style={{paddingVertical: 15}}>
                  <TouchableOpacity onPress={this.goLupaPassword}>
                      <Text style={{textAlign: 'right', color: '#1E90FF', fontStyle:'italic'}}>
                          Lupa kata sandi?
                      </Text>
                  </TouchableOpacity>
            </Block>

            <View style={{paddingTop: 90}}>
            <Block center>
                {/* <TouchableOpacity style={styles.button} onPress={() => this.getDataUsingPost()} >
                  <Text style={styles.buttonText}> MASUK </Text>
                </TouchableOpacity> */}
                <TouchableOpacity style={styles.button} onPress={this.onLogin}>
                  <Text style={styles.buttonText}> M A S U K </Text>
                </TouchableOpacity>
                <TouchableOpacity style={styles.button2} onPress={this.goToSignup}>
                  <Text style={styles.buttonText}> D A F T A R </Text>
                </TouchableOpacity> 
              </Block>
            </View>














        {/* <View style={{ margin: 10 }}>
          <TextInput
            name='email'
            value={email}
            placeholder='Enter email'
            autoCapitalize='none'
            onChangeText={this.handleEmailChange}
          />
        </View> */}
        {/* <View style={{ margin: 10 }}>
          <TextInput
            name='password'
            value={password}
            placeholder='Enter password'
            secureTextEntry
            onChangeText={this.handlePasswordChange}
          />
        </View> */}
        {/* <Button title='Login' onPress={this.onLogin} />
        <Button title='Go to Signup' onPress={this.goToSignup} />
        <Button title='Lupa Kata Sandi' onPress={this.goLupaPassword} /> */}
      </View>
    )
  }
}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent:'center', alignContent:'center' },
  inputBox: { width:300, height: 40, borderColor:'#3CB371', borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 10, paddingVertical: 10 },
  ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' },
  SectionStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 5, margin: 10, width: 300 },
  viewStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 50, margin: 10, width: 350, borderWidth: 1, backgroundColor:'white', paddingHorizontal: 10, borderColor:'#3CB371' },
  button: { width:385, backgroundColor: '#3CB371', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  button2: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },

})