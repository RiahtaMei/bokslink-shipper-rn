import React from 'react'
import { StyleSheet, View, Button, Image, TextInput, TouchableOpacity } from 'react-native'
import { Block, Text } from 'galio-framework';

export default class Signup extends React.Component {

    constructor(props){
        super(props);
        this.state={
          NamaPengguna: '',
          AlamatEmail: '',
          NomorTelepon:'',
          KataSandi:'',
          Konfirmasi_KataSandi:'',
          userData: {},
          jsonOfItem:'',
          message: 'Masukin ulang data ya',
          hidden: true
        }
      }





    goVerifikasi = () => this.props.navigation.navigate('Verifikasi')
    goLogin = () => this.props.navigation.navigate('Login')
    render() {
      return (
        <View style={styles.container}>

            <Block row center style={{paddingVertical: 40}}> 
              <Image  style={{width: 300, height: 100}} source={require('../Image/logo.png')}/>  
            </Block>

            <Block center>
              <View style={styles.SectionStyle}>
                <View style={styles.viewStyle}>
                  <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Nama Pengguna"
                    placeholderTextColor = 'grey'
                    keyboardType="default"
                    value={this.state.NamaPengguna}
                    onChangeText={NamaPengguna => this.setState({ NamaPengguna })}
                  />
                  <Image source={require('../Image/user.png')} style={styles.ImageStyle} /> 
                </View>
              </View>

              <View style={styles.SectionStyle}>
                <View style={styles.viewStyle}>
                  <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Alamat Email"
                    placeholderTextColor = 'grey'
                    keyboardType="email-address"
                    value={this.state.AlamatEmail}
                    onChangeText={AlamatEmail => this.setState({ AlamatEmail })}
                  />
                  <Image source={require('../Image/email.png')} style={styles.ImageStyle} /> 
                </View>
              </View>

              <View style={styles.SectionStyle}>
                <View style={styles.viewStyle}>
                  <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Nomor Telepon"
                    placeholderTextColor = 'grey'
                    keyboardType='phone-pad'
                    value={this.state.NomorTelepon}
                    onChangeText={NomorTelepon => this.setState({ NomorTelepon })}
                  />
                  <Image source={require('../Image/telepon.png')} style={styles.ImageStyle} /> 
                </View>
              </View>

                <View style={styles.viewStyle}>
                  <TextInput 
                    style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Kata Sandi"
                    secureTextEntry={this.state.hidden}
                    {...this.props}
                    placeholderTextColor = 'grey'
                    keyboardType='default'
                    value={this.state.KataSandi}
                    onChangeText={KataSandi => this.setState({ KataSandi })}
                  />
                  <TouchableOpacity onPress={this.onInputLabelPressed}>
                    <Image source={require('../Image/pass.png')} style={styles.ImageStyle} /> 
                  </TouchableOpacity>
                </View>

              <View style={styles.viewStyle}>
                <TextInput 
                  style={styles.inputBox}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  placeholder="Konfirmasi Kata Sandi"
                  secureTextEntry={this.state.hidden}
                  {...this.props}
                  placeholderTextColor = 'grey'
                  keyboardType='default'
                  value={this.state.Konfirmasi_KataSandi}
                  onChangeText={Konfirmasi_KataSandi => this.setState({ Konfirmasi_KataSandi })}
                />
                <TouchableOpacity onPress={this.onInputLabelPressed}>
                  <Image source={require('../Image/pass.png')} style={styles.ImageStyle} /> 
                </TouchableOpacity>
              </View>

              <View style={{paddingTop: 50}}>
                <TouchableOpacity style={styles.button} onPress={this.goVerifikasi} >
                  <Text style={styles.buttonText}> R E G I S T E R </Text>
                </TouchableOpacity>
              </View>
            </Block>












          {/* <Text>Halaman Signup</Text>
          <Button title='Go to Verifikasi' onPress={this.goVerifikasi} />
          <Button title='Go back to Login' onPress={this.goLogin} /> */}
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center' },
  inputBox: { width:300, height: 40, borderColor:'#3CB371', borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 10, paddingVertical: 10 },
  button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
  ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' },
  SectionStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 5, margin: 10, width: 300 },
  viewStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 50, margin: 10, width: 350, borderWidth: 1, backgroundColor:'white', paddingHorizontal: 10, borderColor:'#3CB371' }
})