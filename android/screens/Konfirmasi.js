import React from 'react';
import { StyleSheet, Dimensions, View, TouchableOpacity, ImageBackground } from 'react-native';
import { Text } from 'galio-framework';

export default class Konfirmasi extends React.Component {
    goDetailPengiriman = () => this.props.navigation.navigate('DetailPengiriman')

  render() {
    const { navigation } = this.props;

    const Nama_Pengguna = navigation.getParam('namapengguna', 'No-namapengguna');
    const Alamat_Email = navigation.getParam('alamatemail', 'No-alamatemail');
    const ServiceType = navigation.getParam('servicetype', 'No-servicetype');
    const Subservicetype = navigation.getParam('subservicetype', 'No-subservicetype');  
    const JemputNamaPengguna = navigation.getParam('jemput_NamaPengguna', 'No-jemput_NamaPengguna');
    const JemputNomorTelepon = navigation.getParam('jemput_NomorTelepon', 'No-jemput_NomorTelepon');
    const JemputAlamat = navigation.getParam('jemput_Alamat', 'No-jemput_Alamat');
    const AntarNamaPengguna = navigation.getParam('antar_NamaPengguna', 'No-antar_NamaPengguna');
    const AntarNomorTelepon = navigation.getParam('antar_NomorTelepon', 'No-antar_NomorTelepon');
    const AntarAlamat = navigation.getParam('antar_Alamat', 'No-antar_Alamat');
    const Kategori_Barang = navigation.getParam('KategoriBarang', 'No-KategoriBarang');
    const Sub_Kategori = navigation.getParam('SubKategori', 'No-SubKategori');
    const Deskripsi_ = navigation.getParam('_Deskripsi', 'No-_Deskripsi');
    const Jumlah_Barang = navigation.getParam('JumlahBarang', 'No-JumlahBarang');
    const Total_Harga = navigation.getParam('TotalHarga', 'No-TotalHarga');
    const Panjang_ = navigation.getParam('_Panjang', 'No-_Panjang');  
    const Lebar_ = navigation.getParam('_Lebar', 'No-_Lebar');
    const Tinggi_ = navigation.getParam('_Tinggi', 'No-_Tinggi');
    const Berat_ = navigation.getParam('_Berat', 'No-_Berat');
    const Berat_Setara = navigation.getParam('_BeratSetara', 'No-_BeratSetara');

    const lokasiPenjemputan = navigation.getParam('LokasiPenjemputan', 'No-LokasiPenjemputan');
    const lokasiPengiriman = navigation.getParam('LokasiPengiriman', 'No-LokasiPengiriman');
    const KodePromo = navigation.getParam('kodePromo', 'No-KodePromo');
    const checkBox = navigation.getParam('_checked', 'No-checkbox');
    const RadioButton = navigation.getParam('radioButton', 'No-RadioButton');

    return (
      <View style={styles.home}>
        <View style={{alignContent:'center', justifyContent:'center', alignItems:'center'}}>
        <ImageBackground  style={{width: 450, height: 850}} source={require('../Image/20koin.png')}> 
          <View style={{alignContent:'center', alignItems:'center', justifyContent:'center', paddingTop: 650}}>
            <TouchableOpacity style={styles.button} onPress={this.goDetailPengiriman} >
                <Text style={styles.buttonText}> Ok! </Text>
            </TouchableOpacity>
          </View>
        </ImageBackground>  
        </View>
      </View>
    );
  }
}

const styles = StyleSheet.create({
    home: { flex: 1, paddingTop: 10, alignItems:'center', alignContent:'center', justifyContent:'center', backgroundColor:'white' },
    button: { width: 100, backgroundColor: 'green', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
    buttonText: { fontSize:20, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
});
