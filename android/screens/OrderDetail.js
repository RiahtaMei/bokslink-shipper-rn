const { width } = Dimensions.get('screen');

import { Block, Text, theme } from 'galio-framework';
import React, { Component } from 'react';
import { AsyncStorage } from 'react-native';
import RadioForm, { RadioButton, RadioButtonInput, RadioButtonLabel } from 'react-native-simple-radio-button';

var Pembayaran = [
  {label: "Tunai", value: 'Tunai'},
  {label: "OVO", value: 'OVO'},
  {label: "Go-Pay", value: 'Go-Pay'},
];

import { Linking, Platform, Dimensions, ScrollView, TextInput, Image, TouchableOpacity, View, CheckBox, StyleSheet, KeyboardAvoidingView } from 'react-native';
var tempCheckValues = [];
const { height} = Dimensions.get('screen');

console.ignoredYellowBox = ['Warning: Each', 'Warning: Failed'];

export default class OrderDetail extends React.Component {
  
constructor(props) {
  super(props);
    this.state = {
      checkBoxChecked: [],
      LokasiPenjemputan: '',
      LokasiPengiriman: '',
      KodePromo: '',
      Value:''
      
    }
  }

// dialcall
dialCall = () => {
 
  let phoneNumber = '';

  if (Platform.OS === 'android') {
    phoneNumber = 'tel:${1234567890}';
  }
  else {
    phoneNumber = 'telprompt:${085261966309}';
  }

  Linking.openURL(phoneNumber);
};


goKonfirmasi = () => this.props.navigation.navigate('Konfirmasi')
goBack = () => this.props.navigation.navigate('DetailBarang')

  render() {
    const { navigation } = this.props;

    const alamatemail = navigation.getParam('Email', 'No-Alamat_Email');
    const servicetype = navigation.getParam('serviceType', 'No-serviceType');
    const subservicetype = navigation.getParam('subserviceType', 'No-serviceType');
    const nama_TitikPenjemputan = navigation.getParam('NamaTitikPenjemputan', 'No-NamaPengirim');
    const telepon_TitikPenjemputan = navigation.getParam('TeleponTitikPenjemputan', 'No-NomorTeleponPengirim');
    const titik_Penjemputan = navigation.getParam('TitikPenjemputan', 'No-AlamatPenjemputan');
    const nama_TitikPengiriman = navigation.getParam('NamaTitikPengiriman', 'No-NamaPenerima');
    const telepon_TitikPengiriman = navigation.getParam('TeleponTitikPengiriman', 'No-NomorTeleponPenerima');
    const titik_Pengiriman = navigation.getParam('TitikPengiriman', 'No-AlamatPenerimaan');
    
    const KategoriBarang = navigation.getParam('Kategori_Barang', 'No-Nama_Pengguna');
    const SubKategori = navigation.getParam('Sub_Kategori', 'No-Alamat_Email');
    const _Deskripsi = navigation.getParam('Deskripsi_', 'No-Kata_Sandi');
    const JumlahBarang = navigation.getParam('Jumlah_Barang', 'No-Konfirmasi_Kata_sandi');
    const TotalHarga = navigation.getParam('Total_Harga', 'No-kode_form');
    const _Panjang = navigation.getParam('Panjang_', 'No-KodeVerifikasi');  
    const _Lebar = navigation.getParam('Lebar_', 'No-NamaPengirim');
    const _Tinggi = navigation.getParam('Tinggi_', 'No-NomorTeleponPengirim');
    const _Berat = navigation.getParam('Berat_', 'No-KodePosPengirim');
    const _BeratSetara = navigation.getParam('Berat_Setara', 'No-AlamatPenjemputan');

    var txt;
    if (this.state.checked) {
      txt = 'Rp.37,500.00'
    } else {
      txt = 'Rp.27,500.00'
    }

    let LokasiPenjemputan = titik_Penjemputan;
    let LokasiPengiriman = titik_Pengiriman;
    
    return (
      <ScrollView style={{backgroundColor:'white'}}>
        <KeyboardAvoidingView>
        <View>
      <View style={styles.container}> 
                <View style={{paddingTop: 20, alignItems:'center', alignContent:'center', justifyContent:'center'}}>
                    <View style={styles.SectionStyle}>
                        <Image source={require('../Image/pin.png')} style={styles.ImageStyle} />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            editable={false}
                            value= {LokasiPenjemputan}
                        />
                    </View>
                    </View>
                    <View style={styles.SectionStyle}>
                        <Image source={require('../Image/pinTujuan.png')} style={styles.ImageStyle} />
                        <TextInput style={styles.inputBox}
                            underlineColorAndroid='rgba(0,0,0,0)'
                            editable={false}
                            value= {LokasiPengiriman}
                        />
                    </View>
                 


                    <View style={{paddingVertical: 5, flexDirection: 'row', alignContent:'center', alignItems:'center', justifyContent:'center'}}>
                      <TextInput style={styles.inputBox3}
                              underlineColorAndroid='rgba(0,0,0,0)'
                              placeholder="Masukkan Kode Promo"
                              placeholderTextColor = 'black'
                              selectionColor="#fff"
                              keyboardType="email-address"
                              value={this.state.KodePromo}
                              onChangeText={KodePromo => this.setState({ KodePromo })}
                      />
                    </View>

                    <View style={{paddingTop: 30, paddingBottom: 10,  alignContent:'center', alignItems:'center', justifyContent:'center'}}>
                      <View>
                        <Text>
                        Total yang harus dibayarkan
                        </Text>
                      </View>
                      <View>
                          <View>
                           <TextInput style={styles.inputBox5}  editable={false}>{txt}</TextInput>
                          </View>

                          <View style={{flexDirection:'row', paddingTop: 2}}>
                            <View style={{paddingLeft: 50}}>
                            <CheckBox style={{marginBottom: 25}}
                              value={this.state.checked}
                              onValueChange={(checked) => this.setState({ checked: !this.state.checked })}
                            />
                            </View>
                            <View>
                            <Text style={ styles.checkBoxText}> Asuransi Pengiriman</Text>
                            </View>
                          </View>

                      </View>
                    </View>
                    
                <Block center>
                    <View style={{paddingVertical: 5, alignContent:'center', alignItems:'center', justifyContent:'center'}}>
                        <Text style={{fontWeight:'bold', fontSize: 20, color:'#1E90FF'}}>
                          Pilih Metode Pembayaran:
                        </Text>
                    </View>
                </Block>

                <View style={{paddingTop: 30}}>
                    <RadioForm
                      radio_props={Pembayaran}
                      onPress={Value => this.setState({Value})}
                      buttonSize={15}
                      buttonOuterSize={30}
                      selectedButtonColor={'green'}
                      selectedLabelColor={'green'}
                      labelStyle={{ fontSize: 14, }}
                      disabled={false}
                      formHorizontal={false}
                      style={{flexDirection:'row'}}
                      />
                    </View>


                <View style={{ paddingTop: 20, paddingBottom: 20, alignContent:'center', alignItems:'center', justifyContent:'center'}}> 
                    <View style={{paddingTop: 50}}>
                        <TouchableOpacity style={styles.button} onPress={this.goBack} >
                        <Text style={styles.buttonText}>B A T A L K A N</Text>
                        </TouchableOpacity>
                        <TouchableOpacity style={styles.button2} onPress={this.goKonfirmasi} >
                        <Text style={styles.buttonText2}> K O N F I R M A S I </Text>
                        </TouchableOpacity>
                    </View>
                </View>
        </View>
      </View>
        </KeyboardAvoidingView>
        </ScrollView>
          );
        }
      }
      

const styles = StyleSheet.create({
  container : {
    flexGrow: 1,
    justifyContent:'center',
    alignItems: 'center',
    backgroundColor: 'white'
  },
  tab: {
    backgroundColor: 'white',
    width: width * 0.50,
    borderRadius: 0,
    borderWidth: 0,
    height: 24,
    elevation: 0,
  },
  inputBox3: {
    width:250,
    height: 50,
    borderColor:'#1E90FF',
    borderWidth: 1,
    paddingHorizontal:10,
    fontSize:14,
    color:'black',
    paddingVertical: 10,
    textAlign:'center'
  },
  inputBox4: {
    width:350,
    height: 40,
    paddingHorizontal:10,
    fontSize:20,
    fontWeight:'bold',
    color:'#1E90FF',
    paddingVertical: 0,
    textAlign:'left'
  },
  inputBox5: {
    width:350,
    height:80,
    fontSize:40,
    fontWeight:'bold',
    color:'#3CB371',
    textAlign:'center'
  },
  inputBox6: {
    width:350,
    height: 40,
    paddingHorizontal:10,
    fontSize:20,
    fontWeight:'bold',
    color:'#1E90FF',
    marginVertical: 0,
    paddingVertical: 0,
    textAlign:'center'
  },
  button3:{ 
    width: 140, 
    backgroundColor: '#3CB371',
    paddingVertical: 10, 
    justifyContent: 'center', 
    borderRadius: 25,
    borderColor:'#3CB371',
    borderWidth:1,
  },
  button4:{ 
    width: 140, 
    backgroundColor: 'red',
    paddingVertical: 10, 
    justifyContent: 'center', 
    borderRadius: 25,
    borderColor:'red',
    borderWidth:1,
  },
  buttonText3:{ 
    fontSize: 16, 
    fontWeight: '500', 
    color: 'white', 
    textAlign: 'center', 
    fontWeight: 'bold',
  },
  checkBoxText:{ 
    fontSize: 16, 
    fontWeight: '500', 
    color: '#1E90FF', 
    textAlign: 'center', 
    fontWeight: 'bold',
  },
    SectionStyle: { 
      flexDirection: 'row', 
      justifyContent: 'center', 
      alignItems: 'center',  
      height: 50, 
      borderRadius: 5, 
      margin: 10, 
      width: 350 
  },
  inputBox: { 
    width:300, 
    height: 40, 
    borderColor:'#1E90FF', 
    borderWidth: 1, 
    borderRadius: 25, 
    paddingHorizontal:16, 
    fontSize:16, 
    color:'black', 
    marginVertical: 10, 
    paddingVertical: 10,
},
ImageStyle: { 
  padding: 10, 
  margin: 5, 
  height: 25, 
  width: 25, 
  resizeMode: 'stretch', 
  alignItems: 'center', 
},
button9:{ width: 80, 
  backgroundColor: '#1E90FF',
  paddingVertical: 10, 
  justifyContent: 'center', 
  borderRadius: 10 
},
buttonText9:{ 
  fontSize: 14, 
  fontWeight: '500', 
  color: '#FFFFFF', 
  textAlign: 'center', 
  fontWeight: 'bold' 
},
button: { width:385, backgroundColor: 'red', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
  button2: { width:385, backgroundColor: 'green', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText2: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
  },
  );
