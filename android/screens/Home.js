import React from 'react'
import { StyleSheet, View, Image, TouchableWithoutFeedback, ImageBackground, ScrollView } from 'react-native'
import { Block, Text } from 'galio-framework';
import CardSilder from 'react-native-cards-slider';

export default class Home extends React.Component {

    goForm = () => this.props.navigation.navigate('Form')

    render(){

        const { full, style, imageStyle } = this.props;
        const imageStyles = [styles.image, full ? styles.fullImage : styles.horizontalImage, imageStyle];

        return (
            <ScrollView>

            
            <View style={styles.container}>

                <View style={styles.styleView}>
                    <Text style={styles.styleText1}> Intrakota </Text>
                    <Text style={styles.styleText2}> Wujudkan pengiriman barang Intrakota Anda </Text>
                </View>

                <CardSilder  style={{marginTop: 30}}>
                    <TouchableWithoutFeedback onPress={this.goForm}>
                        <View style={ styles.card1 }>
                            <ImageBackground source={require('../Image/BLD1.jpg')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Intrakota 3 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.goHome2}>
                        <View style={ styles.card2 }>
                        <ImageBackground source={require('../Image/BLD2.jpg')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Intrakota 12 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.goHome2}>
                        <View style={ styles.card3 }>
                        <ImageBackground source={require('../Image/BLD3.png')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Intrakota 24 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </CardSilder>

                <View style={styles.styleView}>
                    <Text style={styles.styleText1}> Interkota </Text>
                    <Text style={styles.styleText2}> Wujudkan pengiriman barang Interkota Anda </Text>
                </View>
                <CardSilder style={{marginTop: 30}}>
                    <TouchableWithoutFeedback onPress={this.goHome2}>
                        <View style={ styles.card2 }>
                            <ImageBackground source={require('../Image/BLD1.jpg')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Interkota 12 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.goHome2}>
                        <View style={ styles.card3 }>
                            <ImageBackground source={require('../Image/BLD1.jpg')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Interkota 24 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>

                    <TouchableWithoutFeedback onPress={this.goHome2}>
                        <View style={ styles.card1 }>
                            <ImageBackground source={require('../Image/BLD1.jpg')} style={[styles.image]}></ImageBackground>
                            <Text style={ styles.styleTextLayanan }>
                            Layanan Interkota 72 jam
                            </Text>
                        </View>
                    </TouchableWithoutFeedback>
                </CardSilder>
            </View>
            </ScrollView>
          );
    }

}

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center' },
  card1: { 
    borderRadius: 3,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    overflow: 'hidden',
    height: 170, 
    justifyContent:'center', 
    alignItems:'center', 
    backgroundColor: 'skyblue'
   },
   card2: { 
    borderRadius: 3,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    overflow: 'hidden',
    height: 170, 
    justifyContent:'center', 
    alignItems:'center', 
    backgroundColor: 'lightsalmon'
   },
   card3: { 
    borderRadius: 3,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    overflow: 'hidden',
    height: 170, 
    justifyContent:'center', 
    alignItems:'center', 
    backgroundColor: 'teal'
   },
   styleText1:{
    fontSize: 23, 
    color: '#000', 
    marginTop:20, 
    marginLeft:20, 
    fontWeight: '500'
   },
   styleText2:{
    fontSize: 14, 
    color: '#000', 
    marginTop:10, 
    marginLeft:20, 
    fontWeight: "200"
   },
   styleTextLayanan:{
    color: 'white', fontSize: 24, fontWeight: 'bold'
   },
   image: {
    borderRadius: 3,
    borderBottomLeftRadius: 15,
    borderBottomRightRadius: 15,
    borderTopRightRadius: 15,
    borderTopLeftRadius: 15,
    overflow: 'hidden',
    height: 120,
    width: 320
  },



})