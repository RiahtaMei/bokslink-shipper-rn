import React from 'react';
// import MapView  from 'react-native-maps';
import { View, Text, Image, TextInput, ScrollView, Dimensions, StyleSheet, Platform, Linking} from 'react-native';
const { height, width } = Dimensions.get('screen');

import { TouchableOpacity, ToolbarAndroid } from 'react-native-gesture-handler';

export default class DetailPengiriman extends React.Component{

  //alamat statis 
  constructor(props) {
    super(props);
    this.state = { 
        Alamat_1: 'RS. Asri Siloam' + '\n' + 'Jalan Duren Tiga Raya No. 15',
        Status_Pengiriman: 'Kurir menerima tugas',
        Nama_Kurir: 'Hermansyah Nugraha Abadi Kusuma',
        Biaya_Pengiriman: ''
    };
  }

  // dialcall
dialCall = () => {
 
  let phoneNumber = '';

  if (Platform.OS === 'android') {
    phoneNumber = 'tel:${1234567890}';
  }
  else {
    phoneNumber = 'telprompt:${1234567890}';
  }

  Linking.openURL(phoneNumber);
};

  render() {
    const { navigation } = this.props;

    const Nama_Pengguna = navigation.getParam('Nama_Pengguna', 'No-Nama_Pengguna');
    const Alamat_Email = navigation.getParam('Alamat_Email', 'No-Alamat_Email');
    const Service_Type = navigation.getParam('ServiceType', 'No-Kata_Sandi');
    const Subservice_Type = navigation.getParam('Subservicetype', 'No-Konfirmasi_Kata_sandi');
    const jemput_NamaPengguna = navigation.getParam('JemputNamaPengguna', 'No-NamaPengirim');
    const jemput_NomorTelepon = navigation.getParam('JemputNomorTelepon', 'No-NomorTeleponPengirim');
    const jemput_Alamat = navigation.getParam('JemputAlamat', 'No-AlamatPenjemputan');
    const antar_NamaPengguna = navigation.getParam('AntarNamaPengguna', 'No-NamaPenerima');
    const antar_NomorTelepon = navigation.getParam('AntarNomorTelepon', 'No-NomorTeleponPenerima');
    const antar_Alamat = navigation.getParam('AntarAlamat', 'No-AlamatPenerimaan');
    const KategoriBarang = navigation.getParam('Kategori_Barang', 'No-Nama_Pengguna');
    const SubKategori = navigation.getParam('Sub_Kategori', 'No-Alamat_Email');
    const _Deskripsi = navigation.getParam('Deskripsi_', 'No-Kata_Sandi');
    const JumlahBarang = navigation.getParam('Jumlah_Barang', 'No-Konfirmasi_Kata_sandi');
    const TotalHarga = navigation.getParam('Total_Harga', 'No-kode_form');
    const _Panjang = navigation.getParam('Panjang_', 'No-KodeVerifikasi');  
    const _Lebar = navigation.getParam('Lebar_', 'No-NamaPengirim');
    const _Tinggi = navigation.getParam('Tinggi_', 'No-NomorTeleponPengirim');
    const _Berat = navigation.getParam('Berat_', 'No-KodePosPengirim');
    const _BeratSetara = navigation.getParam('Berat_Setara', 'No-AlamatPenjemputan');
    const LokasiPenjemputan = navigation.getParam('lokasiPenjemputan', 'No-LokasiPenjemputan');
    const LokasiPengiriman = navigation.getParam('lokasiPengiriman', 'No-LokasiPengiriman');
    const kodePromo = navigation.getParam('KodePromo', 'No-KodePromo');
    const _checked = navigation.getParam('checkBox', 'No-checkbox');
    const radioButton = navigation.getParam('RadioButton', 'No-RadioButton');

  return (
<ScrollView>
  <View style={styles.container}>

  {/* <View>
          <Text>Dari Detail Barang</Text>
          <Text>
                    Nama Pengguna : { JSON.stringify(Nama_Pengguna) }
                  </Text>
                  <Text>
                    Alamat Email : { JSON.stringify(Alamat_Email) }
                  </Text>
                  <Text>
                    Service Type : { JSON.stringify(Service_Type) }
                  </Text>
                  <Text>
                    Sub Service Type : { JSON.stringify(Subservice_Type) }
                  </Text>
                  <Text>
                    Nama Pengirim : {JSON.stringify(jemput_NamaPengguna)}
                  </Text>
                  <Text>
                    Nomor Telepon Pengirim : {JSON.stringify(jemput_NomorTelepon)}
                  </Text>
                  <Text>
                    Alamat Pengiriman : {JSON.stringify(jemput_Alamat)}
                  </Text>
                  <Text>
                    Nama Penerima : {JSON.stringify(antar_NamaPengguna)}
                  </Text>
                  <Text>
                    Nomor Telepon Penerima : {JSON.stringify(antar_NomorTelepon)}
                  </Text>
                  <Text>
                    Alamat Penerimaan : {JSON.stringify(antar_Alamat)}
                  </Text>

                  <Text>
                    Kategori Barang : {JSON.stringify(KategoriBarang)}
                  </Text>
                  <Text>
                    Sub-Kategori : {JSON.stringify(SubKategori)}
                  </Text>
                  <Text>
                    Deskripsi : {JSON.stringify(_Deskripsi)}
                  </Text>
                  <Text>
                    Jumlah Barang : {JSON.stringify(JumlahBarang)}
                  </Text>
                  <Text>
                    Total Harga : {JSON.stringify(TotalHarga)}
                  </Text>
                  <Text>
                    Panjang : {JSON.stringify(_Panjang)}
                  </Text>
                  <Text>
                    Lebar : { JSON.stringify(_Lebar) }
                  </Text>
                  <Text>
                    Tinggi : { JSON.stringify(_Tinggi) }
                  </Text>
                  <Text>
                    Berat : { JSON.stringify(_Berat) }
                  </Text>
                  <Text>
                    Berat Setara : { JSON.stringify(_BeratSetara) }
                  </Text>
                  <Text>
                    Lokasi Penjemputan : {JSON.stringify(LokasiPenjemputan)}
                  </Text>
                  <Text>
                    Lokasi Pengiriman : { JSON.stringify(LokasiPengiriman) }
                  </Text>
                  <Text>
                    Kode Promo : { JSON.stringify(kodePromo) }
                  </Text>
                  <Text>
                    Checkbox : { JSON.stringify(_checked) }
                  </Text>
                  <Text>
                    Radio Button : { JSON.stringify(radioButton) }
                  </Text>
                </View> */}

              
    <View style={{paddingTop: 1}}>
      {/* <MapView
        style={{ alignSelf: 'stretch', height: 320 }}
        region={{
          latitude: -6.2544499,
          longitude: 106.8326136,
          latitudeDelta: 0.0922,
          longitudeDelta: 0.0421,
        }}
      >
      <MapView.Marker
      coordinate={{
        latitude: -6.2544499,
        longitude: 106.8326136,
      }}
      title={'My marker title'}
      description={'My marker description'}
      />
      </MapView> */}
    </View>
    <View style={{paddingTop: 0}}>
      <View style={{backgroundColor:'#1faadb', alignContent:'center', justifyContent:'center', alignItems:'center', height: 80}}>
        <View style={{paddingTop: 10, alignContent:'center', alignItems:'center', justifyContent:'center'}}>          
          <View style={{flexDirection: 'row', justifyContent: 'center', alignItems: 'center',  height: 50, borderRadius: 5, margin: 5, width: 300, alignContent:'center'}}>
            <View>
              <Image source={require('../Image/placeholder2.png')} style={{padding: 0, margin: 5, height: 30, width: 30, resizeMode: 'stretch', alignItems: 'center', alignContent:'center', justifyContent:'center'}} />
            </View>
            <TextInput style={{width:200, height: 80, paddingHorizontal:5, fontSize:16, color:'#3CB371', marginVertical: 10, paddingVertical: 10, fontSize: 16,textAlign:'left', alignContent:'center', alignItems:'center', justifyContent:'center', color:'white', fontWeight:'bold', fontSize: 14}}
              underlineColorAndroid='rgba(0,0,0,0)'
              multiline={true}
              value={this.state.Alamat_1}
              editable={false}
            />
            <View>
              <TouchableOpacity onPress={this.dialCall} activeOpacity={0.7}>
                <Image source={require('../Image/telephone.png')} style={{padding: 3, margin: 5, height: 30, width: 30, resizeMode: 'stretch', alignItems: 'center', alignContent:'center', justifyContent:'center'}} />
              </TouchableOpacity>
            </View>
            <View>
              <TouchableOpacity onPress={() => navigation.navigate('Chat')}>
                <Image source={require('../Image/msg.png')} style={{padding: 3, margin: 5, height: 30, width: 30, resizeMode: 'stretch', alignItems: 'center', alignContent:'center', justifyContent:'center'}} />
              </TouchableOpacity>
            </View>
          </View>
        </View>
      </View>
    </View>
    
    <View style={{alignItems:'center', alignContent:'center', justifyContent:'center'}}>
      <View style={styles.SectionStyle}>           
        <Image source={require('../Image/status.png')} style={styles.ImageStyle} />
        <TextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          value={this.state.Status_Pengiriman}
          editable={false}
        />                
      </View>
      <Text>Status Pengiriman</Text> 
      <View style={styles.SectionStyle}>      
        <Image source={require('../Image/shipping.png')} style={styles.ImageStyle} />
        <TextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          value={this.state.Nama_Kurir}
          keyboardType="default"
          editable={false}
        />
      </View>
      <Text>Nama Kurir</Text>
      <View style={styles.SectionStyle}>
        <Image source={require('../Image/rupiahs.png')} style={styles.ImageStyle} />
        <TextInput style={styles.inputBox}
          underlineColorAndroid='rgba(0,0,0,0)'
          placeholder="Rp. 37.650 (Tunai)"
          placeholderTextColor = '#3CB371'
          selectionColor="#fff"
          keyboardType="default"
          editable={false}
        />
      </View>
      <Text>Biaya Pengiriman</Text>  
      <View style={{paddingTop:20, paddingBottom:20}}>
        <TouchableOpacity style = {styles.button4} onPress={() => navigation.navigate('Batal')}>
          <Text style= {styles.buttonText2}> BATALKAN </Text>
        </TouchableOpacity>
      </View>
    </View>
  </View>
</ScrollView>
    );
  }
  
}

// Styles
  const styles = StyleSheet.create({
    container: {flex: 0.8, backgroundColor: 'white' },
    button:{ width: 300, backgroundColor: '#F0FFF0', borderColor: '#1E90FF',paddingVertical: 10, justifyContent: 'center', borderWidth:1 },
    button2:{ width: 180, backgroundColor: '#1E90FF',paddingVertical: 10, justifyContent: 'center', borderRadius: 25 },
    button4:{ width: 180, backgroundColor: 'red',paddingVertical: 10, justifyContent: 'center', borderRadius: 25 },
    buttonText:{ fontSize: 16, fontWeight: '500', color: '#1E90FF', textAlign: 'center', fontWeight: 'bold' },
    iconStyle:{ flexDirection:"row", justifyContent:'space-between', padding: 20 },
    ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center', },
    SectionStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center',  height: 50, borderRadius: 5, margin: 10, width: 300 },
    inputBox: { width:300, height: 40, borderColor:'#1E90FF', borderBottomWidth: 2, borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'#3CB371', marginVertical: 10, paddingVertical: 10 },
    buttonText2: { fontSize: 16, fontWeight: '500', color: '#FFFFFF', textAlign: 'center', fontWeight: 'bold', justifyContent:'center', alignItems:'center', alignContent:'center' },
    button_3: { width: 70, backgroundColor: '#1E90FF', padding:5 },
  });







