
import React, {Component} from 'react';
import { StyleSheet,Text, View, Image, TextInput, TouchableOpacity} from 'react-native';
import SearchableDropdown from 'react-native-searchable-dropdown';
import styled from 'styled-components'
import { ScrollView } from 'react-native-gesture-handler';


import { YellowBox } from 'react-native'



YellowBox.ignoreWarnings([
  'VirtualizedLists should never be nested', // TODO: Remove when fixed
])

const Container=styled.View`
    flex:1;
    padding:50px 0;
    justify-content:center;
    background-color:white;
    align-items:center
`
const Title=styled.Text`
font-size:20px;
text-align:center;
 color:red;
`
const Item=styled.View`
flex:1;
border:1px solid #ccc;
margin:2px 0;
border-radius:10px;
box-shadow:0 0 10px #ccc;
background-color:#fff;
width:90%;
padding:10px;
`


export default class Form extends Component {
  constructor() {
    super();
    this.state = {
      serverData: [],
      nama_titikPenjemputan:'',
      telepon_titikPenjemputan:'',
      item:'',
      titik_Penjemputan:''
    };
  }
  
    itemSelect(item) {
      this.setState({titik_Penjemputan: item.name});
    }

  componentDidMount() {
    fetch('http://192.168.1.138/api/Search/searchLocation')
      .then(response => response.json())
      .then(responseJson => {
        //Successful response from the API Call
        this.setState({
          serverData: [...this.state.serverData, ...responseJson.results],
          //adding the new data in Data Source of the SearchableDropdown
        });
      })
      .catch(error => {
        console.error(error);
      });
  }

  goDetailBarang = () => this.props.navigation.navigate('DetailBarang')

  render() {
    return (
        <ScrollView>
            <Container>
                    <Item>
                        <ScrollView>
                            <View style={{alignContent:'center', alignItems:'center', justifyContent:'center', paddingTop: 10}}>
                                <View style={{borderWidth:1, backgroundColor:'#1faadb', height:50,textAlign:'center', justifyContent:'center', alignItems:'center', alignContent:'center', borderColor:'#1faadb', paddingHorizontal: 40, borderRadius: 15}}>
                                    <Text style={{fontWeight:'bold', color:'white', fontSize: 14}}>
                                        TITIK PENJEMPUTAN BARANG
                                    </Text>
                                </View>
                            </View>
                            <View style={{backgroundColor:'white', flex:1}}>
                                <View>        
                                    <View style={{paddingTop: 20}}>
                                        <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                            <Image source={require('../Image/people.png')} style={styles.ImageStyle} />
                                        </View>
                                        <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                            <TextInput style={styles.inputBox}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                placeholder="Nama Pengirim"
                                                placeholderTextColor = "#c7c6c1"
                                                selectionColor="#fff"
                                                keyboardType="default"  
                                                value={this.state.nama_titikPenjemputan}
                                                onChangeText={nama_titikPenjemputan => this.setState({nama_titikPenjemputan})}  
                                            />
                                        </View>
                                    </View>
                                    <View style={{paddingTop: 20}}>
                                        <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                            <Image source={require('../Image/pin.png')} style={styles.ImageStyle} />
                                        </View>
                                        <View>
                                            <SearchableDropdown
                                                onTextChange={text => console.log(text)}
                                                onItemSelect={ item => this.itemSelect(item) }
                                                // value={this.state.titik_Penjemputan}
                                                // onChangeText={titik_Penjemputan => this.setState(titik_Penjemputan)}
                                                
                                                containerStyle={{ padding: 5 }}
                                                //suggestion container style
                                                textInputStyle={{
                                                    //inserted text style
                                                    padding: 8,
                                                    // borderWidth: 1,
                                                    // borderBottomWidth: 1,
                                                    // bordeBottomColor: '#1E90FF',
                                                    borderColor: '#1faadb',
                                                    backgroundColor: 'white',
                                                    // width: 300
                                                }}
                                                itemStyle={{
                                                    //single dropdown item style
                                                    padding: 2,
                                                    marginTop: 2,
                                                    backgroundColor: '#FAF9F8',
                                                    borderColor: '#bbb',
                                                    borderWidth: 1,
                                                }}
                                                itemTextStyle={{
                                                    //single dropdown item's text style
                                                    color: '#222',
                                                }}
                                                itemsContainerStyle={{
                                                    //items container style you can pass maxHeight
                                                    //to restrict the items dropdown hieght
                                                    maxHeight: '47%',
                                                }}
                                                items={this.state.serverData}
                                                //mapping of item array
                                                defaultIndex={2}
                                                //default selected item index
                                                placeholder="Lokasi Penjemputan Barang"
                                                //place holder for the search input
                                                resetValue={false}
                                                //reset textInput Value with true and false state
                                                underlineColorAndroid="transparent"
                                                //To remove the underline from the android input
                                                fontSize='14'
                                                textAlign='center'
                                                alignItems='center'
                                                justifyContent='center'
                                                style={{textAlign:'center', alignContent:'center', alignItems:'center', justifyContent:'center'}}
                                            />
                                        </View>
                                    </View>

                                    <View style={{paddingTop: 20}}>
                                        <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                            <Image source={require('../Image/phone.png')} style={styles.ImageStyle} />
                                        </View>
                                        <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                            <TextInput style={styles.inputBox}
                                                underlineColorAndroid='rgba(0,0,0,0)'
                                                placeholder="Nomor Telepon Pengirim"
                                                placeholderTextColor = "#c7c6c1"
                                                selectionColor="#fff"
                                                keyboardType="phone-pad"
                                                value={this.state.telepon_titikPenjemputan}
                                                onChangeText={telepon_titikPenjemputan => this.setState({telepon_titikPenjemputan})}
                                            />
                                        </View>
                                    </View>  
                                </View>
                                <View style={{paddingTop: 50, alignItems:'center', alignContent:'center', justifyContent:'center', paddingBottom: 20}}></View>
                            </View>
                        </ScrollView>
                    </Item>

                    <View style={{paddingBottom: 20}}></View>

                    <Item>
                    <ScrollView>
                        <View style={{alignContent:'center', alignItems:'center', justifyContent:'center', paddingTop: 10}}>
                            <View style={{borderWidth:1, backgroundColor:'#1faadb', height:50,textAlign:'center', justifyContent:'center', alignItems:'center', alignContent:'center', borderColor:'#1faadb', paddingHorizontal: 40, borderRadius: 15}}>
                                <Text style={{fontWeight:'bold', color:'white', fontSize: 14}}>
                                    TITIK PENGIRIMAN BARANG
                                </Text>
                            </View>
                        </View>
                        <View style={{backgroundColor:'white', flex:1}}>
                            <View>        
                                <View style={{paddingTop: 20}}>
                                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                        <Image source={require('../Image/people.png')} style={styles.ImageStyle} />
                                    </View>
                                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                        <TextInput style={styles.inputBox}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder="Nama Penerima"
                                            placeholderTextColor = "#c7c6c1"
                                            selectionColor="#fff"
                                            keyboardType="default"  
                                            value={this.state.nama_titikPenjemputan}
                                            onChangeText={nama_titikPenjemputan => this.setState({nama_titikPenjemputan})}  
                                        />
                                    </View>
                                </View>
                                <View style={{paddingTop: 20}}>
                                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                        <Image source={require('../Image/pin.png')} style={styles.ImageStyle} />
                                    </View>
                                    <View>
                                        <SearchableDropdown
                                            onTextChange={text => console.log(text)}
                                            onItemSelect={ item => this.itemSelect(item) }
                                            // value={this.state.titik_Penjemputan}
                                            // onChangeText={titik_Penjemputan => this.setState(titik_Penjemputan)}
                                            
                                            containerStyle={{ padding: 5 }}
                                            //suggestion container style
                                            textInputStyle={{
                                                //inserted text style
                                                padding: 8,
                                                // borderWidth: 1,
                                                // borderBottomWidth: 1,
                                                // bordeBottomColor: '#1E90FF',
                                                borderColor: '#1faadb',
                                                backgroundColor: 'white',
                                                // width: 300
                                            }}
                                            itemStyle={{
                                                //single dropdown item style
                                                padding: 2,
                                                marginTop: 2,
                                                backgroundColor: '#FAF9F8',
                                                borderColor: '#bbb',
                                                borderWidth: 1,
                                            }}
                                            itemTextStyle={{
                                                //single dropdown item's text style
                                                color: '#222',
                                            }}
                                            itemsContainerStyle={{
                                                //items container style you can pass maxHeight
                                                //to restrict the items dropdown hieght
                                                maxHeight: '47%',
                                            }}
                                            items={this.state.serverData}
                                            //mapping of item array
                                            defaultIndex={2}
                                            //default selected item index
                                            placeholder="Lokasi Penerimaan Barang"
                                            //place holder for the search input
                                            resetValue={false}
                                            //reset textInput Value with true and false state
                                            underlineColorAndroid="transparent"
                                            //To remove the underline from the android input
                                            fontSize='14'
                                            textAlign='center'
                                            alignItems='center'
                                            justifyContent='center'
                                            style={{textAlign:'center', alignContent:'center', alignItems:'center', justifyContent:'center'}}
                                        />
                                    </View>
                                </View>

                                <View style={{paddingTop: 20}}>
                                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                        <Image source={require('../Image/phone.png')} style={styles.ImageStyle} />
                                    </View>
                                    <View style={{justifyContent:'center', alignContent:'center', alignItems:'center'}}>
                                        <TextInput style={styles.inputBox}
                                            underlineColorAndroid='rgba(0,0,0,0)'
                                            placeholder="Nomor Telepon Penerima"
                                            placeholderTextColor = "#c7c6c1"
                                            selectionColor="#fff"
                                            keyboardType="phone-pad"
                                            value={this.state.telepon_titikPenjemputan}
                                            onChangeText={telepon_titikPenjemputan => this.setState({telepon_titikPenjemputan})}
                                        />
                                    </View>
                                </View>  
                            </View>
                            <View style={{paddingTop: 50, alignItems:'center', alignContent:'center', justifyContent:'center', paddingBottom: 20}}></View>
                        </View>
                    </ScrollView>
                </Item>

                <View style={{paddingTop: 50}}>
                <TouchableOpacity style={styles.button} onPress={this.goDetailBarang} >
                  <Text style={styles.buttonText}> L A N J U T K A N </Text>
                </TouchableOpacity>
              </View>



        </Container>
    </ScrollView>


    );
  }
}


const styles = StyleSheet.create({

  input: {maxHeight: 10},
  container: {
    flex: 0.8, 
    alignItems: 'center', 
    justifyContent: 'center', 
    backgroundColor: 'white'
},
ImageStyle: { 
    padding: 10, 
    marginLeft: 10, 
    height: 25, 
    width: 25, 
    resizeMode: 'stretch', 
    alignItems: 'center', 
    alignContent:'center',
    justifyContent:'center'
},
inputBox: { 
    width:450, 
    height: 40, 
    borderColor:'#1faadb', 
    // borderBottomWidth: 1, 
    borderRadius: 25, 
    paddingHorizontal:16, 
    fontSize:16, 
    color:'black', 
    marginVertical: 10, 
    paddingVertical: 10,
    textAlign:'center', 
},
button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
buttonText: { fontSize:20, fontWeight:'bold', color:'#ffffff', textAlign:'center' },

});