import React from 'react'
import { StyleSheet, View, Button, Image, TouchableOpacity, TextInput } from 'react-native'
import { Block, Text } from 'galio-framework';

export default class Verifikasi extends React.Component {


  constructor(props){
    super(props);
    this.state={
      KodeVerifikasi: '',
      // alamatEmail: 'riahtameik@gmail.com',
      userData: {},
      jsonOfItem:'',
      message: 'Masukin ulang data ya',
    }
  }


    goSignup = () => this.props.navigation.navigate('Signup')
    goLogin = () => this.props.navigation.navigate('Login')

    render() {
      return (
        <View style={styles.container}>
            <Block row center style={{paddingVertical: 40}}> 
              <Image  style={{width: 300, height: 100}} source={require('../Image/logo.png')}/>  
            </Block>

            <Block center>
                  <View style={{paddingTop: 40, alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}>
                    <Text style={{textAlign: 'center', color: '#1E90FF', fontSize: 18}}> Konfirmasi OTP </Text>
                    <Text style={{textAlign: 'center', color: '#1E90FF', fontSize: 18}}> Kode OTP udah dikirim ke Email kamu ya </Text>
                  </View>


                  <View>
                  <View style={styles.viewStyle}>
                  <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Kode OTP"
                    placeholderTextColor = 'grey'
                    keyboardType= 'default'
                    value={this.state.KodeVerifikasi}
                    onChangeText={KodeVerifikasi => this.setState({ KodeVerifikasi })}
                  />
                  <Image source={require('../Image/email.png')} style={styles.ImageStyle} /> 
                </View>
                  </View>


                  {/* <View style={ styles.bottom }>
                    <TouchableOpacity style={styles.button} onPress={this.goLogin} >
                        <Text style={styles.buttonText}> KONFIRMASI </Text>
                    </TouchableOpacity> 
                  </View> */}

                  <View style={ {textAlign: "center",justifyContent:'center', bottom:0, alignItems:'center', paddingTop: 10} }>
                    <TouchableOpacity style={styles.button} onPress={this.goLogin} >
                        <Text style={styles.buttonText}> K O N F I R M A S I  O T P </Text>
                    </TouchableOpacity> 
                  </View>
                </Block>


          




          {/* <Text>Halaman Verifikasi</Text>
          <Button title='Go back to Signup screen' onPress={this.goSignup} /> */}
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', paddingTop: ( Platform.OS === 'ios' ) ? 20 : 0 },
  inputBox: { width:300, height: 40, borderColor:'#3CB371', borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 10, paddingVertical: 10 },
  button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
  ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' },
  SectionStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 5, margin: 10, width: 300 },
  viewStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 50, margin: 10, width: 350, borderWidth: 1, backgroundColor:'white', paddingBottom: 10, borderColor:'#3CB371' },
  bottom:{ textAlign: "center",justifyContent:'center', flex:1, justifyContent:'flex-end', marginBottom: 36 }
})