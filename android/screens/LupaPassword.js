import React from 'react'
import { StyleSheet, View, Button, Image, TextInput, TouchableOpacity } from 'react-native'
import { Block, Text } from 'galio-framework';

export default class Verifikasi extends React.Component {


  constructor(props){
    super(props);
    this.state={
      AlamatEmail: '',
      userData: {},
      jsonOfItem:'',
      message: 'Silahkan masukkan ulang Email ya',
    }
  }


    goOTPpemulihan = () => this.props.navigation.navigate('OTPpemulihan')
    render() {
      return (
        <View style={styles.container}>

            <Block row center style={{paddingVertical: 40}}> 
                <Image  style={{width: 300, height: 100}} source={require('../Image/logo.png')}/>  
            </Block>

            <View style={{paddingTop: 40, alignItems: 'center', alignContent: 'center', justifyContent: 'center'}}>
                  <Text style={{textAlign: 'center', color: '#1E90FF', fontSize: 18}}> Reset Kata Sandi Anda </Text>
                  <Text style={{textAlign: 'center', color: '#1E90FF', fontSize: 18}}> Silahkan input email pemulihan Anda </Text>
                </View>

            <View style={styles.viewStyle}>
                  <TextInput style={styles.inputBox}
                    underlineColorAndroid='rgba(0,0,0,0)'
                    placeholder="Alamat Email"
                    placeholderTextColor = 'grey'
                    keyboardType="email-address"
                    value={this.state.AlamatEmail}
                    onChangeText={AlamatEmail => this.setState({ AlamatEmail })}
                  />
                  <Image source={require('../Image/email.png')} style={styles.ImageStyle} /> 
            </View>

            <View style={{paddingTop: 50}}>
                <TouchableOpacity style={styles.button} onPress={this.goOTPpemulihan} >
                  <Text style={styles.buttonText}> K I R I M </Text>
                </TouchableOpacity>
              </View>







          {/* <Text>Halaman Lupa Kata Sandi</Text>
          <Button title='Go to konfirmasi OTP' onPress={this.goOTPpemulihan} /> */}
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems:'center' },
  viewStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 50, margin: 10, width: 350, borderWidth: 1, backgroundColor:'white', paddingHorizontal: 10, borderColor:'#3CB371' },
  ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' },
  inputBox: { width:300, height: 40, borderColor:'#3CB371', borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 10, paddingVertical: 10 },
  button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
})