import React from 'react'
import { StyleSheet, Image, View, Button, TouchableOpacity, TextInput } from 'react-native'
import { Block, Text } from 'galio-framework';

export default class Verifikasi extends React.Component {

  constructor(props){
    super(props);
    this.state={
      KataSandiBaru: '',
      Konfirmasi: '',
      // alamatEmail:'riahtameik@gmail.com',
      // kodeVerifikasi: '1234',
      hidden: true,
      userData: {},
      jsonOfItem:'',
      message: 'Silahkan masukkan ulang Email dan Passwordnya ya',
    }
  }


  onInputLabelPressed = () => {
    this.setState({hidden: !this.state.hidden});
  }


    goLogin = () => this.props.navigation.navigate('Login')
    render() {
      return (
        <View style={styles.container}>

            <Block row center style={{paddingVertical: 40}}> 
                <Image  style={{width: 300, height: 100}} source={require('../Image/logo.png')}/>  
            </Block>

            <View style={styles.viewStyle}>
                <TextInput 
                  style={styles.inputBox}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  placeholder="Kata Sandi Baru"
                  secureTextEntry={this.state.hidden}
                  {...this.props}
                  placeholderTextColor = 'grey'
                  keyboardType='default'
                  value={this.state.KataSandiBaru}
                  onChangeText={KataSandiBaru => this.setState({ KataSandiBaru })}
                />
                <TouchableOpacity onPress={this.onInputLabelPressed}>
                  <Image source={require('../Image/pass.png')} style={styles.ImageStyle} />
                </TouchableOpacity>
          </View>


          <View style={styles.viewStyle}>
                <TextInput 
                  style={styles.inputBox}
                  underlineColorAndroid='rgba(0,0,0,0)'
                  placeholder="Konfirmasi Kata Sandi Baru"
                  secureTextEntry={this.state.hidden}
                  {...this.props}
                  placeholderTextColor = 'grey'
                  keyboardType='default'
                  value={this.state.Konfirmasi}
                  onChangeText={Konfirmasi => this.setState({ Konfirmasi })}
                />
                <TouchableOpacity onPress={this.onInputLabelPressed}>
                  <Image source={require('..//Image/pass.png')} style={styles.ImageStyle} />
                </TouchableOpacity>
          </View>

          <View style={{paddingTop: 50}}>
                <TouchableOpacity style={styles.button} onPress={this.goLogin} >
                  <Text style={styles.buttonText}> K O N F I R M A S I  K A T A  S A N D I </Text>
                </TouchableOpacity>
              </View>




          {/* <Text>Halaman Ganti Kata Sandi</Text>
          <Button title='Go back to Login Screen' onPress={this.goLogin} /> */}
        </View>
      )
    }
  }

const styles = StyleSheet.create({
  container: { flex: 1, backgroundColor: '#fff', justifyContent: 'center', alignItems:'center' },
  inputBox: { width:300, height: 40, borderColor:'#3CB371', borderRadius: 25, paddingHorizontal:16, fontSize:16, color:'black', marginVertical: 10, paddingVertical: 10 },
  ImageStyle: { padding: 10, margin: 5, height: 25, width: 25, resizeMode: 'stretch', alignItems: 'center' },
  SectionStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 5, margin: 10, width: 300 },
  viewStyle: { flexDirection: 'row', justifyContent: 'center', alignItems: 'center', height: 50, borderRadius: 50, margin: 10, width: 350, borderWidth: 1, backgroundColor:'white', paddingHorizontal: 10, borderColor:'#3CB371' },
  button: { width:385, backgroundColor: '#1E90FF', borderRadius: 10, marginVertical: 5, paddingVertical: 8 },
  buttonText: { fontSize:16, fontWeight:'bold', color:'#ffffff', textAlign:'center' },
})